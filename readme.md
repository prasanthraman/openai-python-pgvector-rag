---
title: Retrieval Augumented Generation
---
# Retrieval Augumented Generation:
 RAG is a technique used in AI applications where custom specific knowledge (not available within LLM) is stored in a vector database and based on user query that knowledge is retrieved from database using similarity seach algorithms. 

 ![RAG illustration](./assets/rag.png "RAG")

Query from user -> search that query in Vector DB for context -> Context retrived based on similarity search score (top – k) results -> prompt construction based on context -> Then get the Response from LLM with our aditional context resulting in more accurate result.

 ![RAG Architecture](./assets/rag%20architecture.png "RAG")


