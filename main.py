import openai
import pinecone      
import os
from dotenv import load_dotenv
from PG_dbHandler import PgDatabaseHandler


def prompt_construction(query,context):
    template = """
    Please try to answer the query with given context
    if you don't know, try to give partial hints with context. Try your best to give answers event if context is not enough. \"\n\nP:{context}\nQ: {query}\nA:
    """
    final_prompt = template.format(
        context=context,
        query=query
    )
    return final_prompt

if __name__=="__main__":
    load_dotenv()
    pg_handler = PgDatabaseHandler()
    pg_handler.connect()
    openai.api_key = os.getenv('API_KEY')
    openai.api_base = os.getenv('API_BASE') 
    openai.api_type = os.getenv('API_TYPE')
    openai.api_version = os.getenv('API_VERSION') 
    
    query = "What is the secret code?"

    
    context = pg_handler.search_relevant_data(query)
    print(context)

    final_prompt = prompt_construction(query,context)
    response = openai.ChatCompletion.create(
            engine="gpt-35-turbo-sr",
            messages=[
             {"role": "user", "content": final_prompt}
            ],
            )
   
    print(response.choices[0].message)

