import openai
import requests
import pinecone  
import os
import PyPDF2
from dotenv import load_dotenv
from openai.embeddings_utils import get_embedding
from PG_dbHandler import PgDatabaseHandler

def create_embeddings_and_feed(chunks):
    for index in range(0,len(chunks)):
        information=chunks[index]
        
        response = get_embedding(information, "text-embedding-ada-002-sr")
        # print(response)
        insertToVectorDB(response, information,'test-id')

def read_pdf_text(file_path):
    text = ""
    try:
        with open(file_path, 'rb') as file:
            pdf_reader = PyPDF2.PdfReader(file)
            num_pages = len(pdf_reader.pages)
            for page_num in range(num_pages):
                page = pdf_reader.pages[page_num]
                text += page.extract_text()
    except Exception as e:
        print("Error reading PDF file:", e)
    return text

def insertToVectorDB(embedding,context,id):
    upsert_response = pg_handler.insert_embedding_pg(context, embedding)
    print(upsert_response)

if __name__=="__main__":
    load_dotenv()
    pg_handler = PgDatabaseHandler()
    pg_handler.connect()
    openai.api_key = os.getenv('API_KEY')
    openai.api_base = os.getenv('API_BASE') 
    openai.api_type = os.getenv('API_TYPE')
    openai.api_version = os.getenv('API_VERSION') 
 
    
    text = read_pdf_text('docs/test.pdf')
    # # Check if the request was successful (status code 200)
    # if pdf_response.status_code == 200:
    #     # Create a PDF object from the downloaded content
    #    text=pdf_response.text
    #    print(text)
    # else:
    #     print("Failed to download the PDF. Status code:", pdf_response.status_code)

    # printing original string
    print("The original string is : " + str(text))

    # compute chunk length
    chnk_len = 6000
    
    # initialize an empty list to store the chunks
    chunks = []
    
    # loop through the string and slice it into chunks of length chnk_len
    for i in range(0, len(text), chnk_len):
        chunks.append(text[i:i+chnk_len])
    
    
    print("The K len chunked list : " + str(chunks), "Len : "+ str(len(chunks)))

    create_embeddings_and_feed(chunks)
