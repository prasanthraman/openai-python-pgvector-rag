import psycopg2
import os
import pgvector
import numpy as np
from psycopg2.extras import execute_values
from pgvector.psycopg2 import register_vector
from openai.embeddings_utils import get_embedding

class PgDatabaseHandler:
    def __init__(self):

        db_params = {
            'host': os.getenv('PG_HOST'),

            'database': os.getenv('PG_DATABASE'),
            'user': os.getenv('PG_USER'),
            'password': os.getenv('PG_PASSWORD')
        }

        self.db_params = db_params
        self.conn = None
        self.cur = None

    def connect(self):
        try:
            self.conn = psycopg2.connect(**self.db_params)
            self.cur = self.conn.cursor()
            register_vector(self.cur)
            
        except psycopg2.Error as e:
            print("Error: Unable to connect to the database")
            print(e)

    def insert_embedding(self, content : list):
        if not self.conn or not self.cur:
            print("Error: Database connection is not established.")
            return

        try:
            for data in content:
                embedding = get_embedding(data, settings.EMBEDDING_MODEL)
                embedding = np.array(embedding)
                # insert_query = "INSERT INTO chandrayaan (content, embedding) VALUES (%s, %s);"
                insert_query =  """
                                INSERT INTO document_embeddings (content, page_no, embedding)
                                VALUES (%s, %s, %s)
                                """
                data_to_insert = (data,None,embedding)
                self.cur.execute(insert_query, data_to_insert)
                self.conn.commit()
                print("INFO:     Data inserted successfully")

            return True
        except psycopg2.Error as e:
            print("Error: Unable to insert data")
            print(e)
            self.conn.rollback()

    def insert_embedding_pg(self, data : str, embedding : list):
        if not self.conn or not self.cur:
            print("Error: Database connection is not established.")
            return

        try:
            
            
            embedding = np.array(embedding)
            # insert_query = "INSERT INTO chandrayaan (content, embedding) VALUES (%s, %s);"
            insert_query =  """
                            INSERT INTO document_embeddings (content, page_no, embedding)
                            VALUES (%s, %s, %s)
                            """
            data_to_insert = (data,None,embedding)
            self.cur.execute(insert_query, data_to_insert)
            self.conn.commit()
            print("INFO:     Data inserted successfully")

            return True
        except psycopg2.Error as e:
            print("Error: Unable to insert data")
            print(e)
            self.conn.rollback()

    def search_relevant_data(self, msg: str):
            # Execute a SELECT query
        query_embedding = get_embedding(msg,"text-embedding-ada-002-sr")
        query_embedding_np = np.array(query_embedding)
        query = '''
                    WITH query_embedding AS (
                SELECT %s::numeric[] AS query
            )  -- Replace this query with your own query vector

            SELECT 
                d.content
            FROM
                vectortable d
            CROSS JOIN
                query_embedding
            ORDER BY
                (
                    SELECT SUM(a * b) / (SQRT(SUM(a * a)) * SQRT(SUM(b * b)))
                    FROM unnest(query_embedding.query, d.embedding) AS x(a, b)
                ) DESC
            LIMIT 3;
        '''
        cosine_query = """
                        SELECT id, content, page_no, 1 - (embedding <=> %s) AS similarity
                        FROM document_embeddings
                        ORDER BY similarity DESC
                        LIMIT %s;
                        """
        eucledian_query = """
                            SELECT id, content, page_no, 1 - (embedding <-> %s) AS similarity
                            FROM document_embeddings
                            ORDER BY similarity DESC
                            LIMIT %s;
                            """
        
        # Execute the query and pass the query_embedding as a parameter
        # self.cur.execute(query, (query_embedding,))
        self.cur.execute(cosine_query,(query_embedding_np,3)) #change top_k=5
        # Fetch the results
        results = self.cur.fetchall()

        return results

    def delete_embeddings_for_document(self, id: int):
        if not self.conn or not self.cur:
            print("Error: Database connection is not established.")
            return

        try:
            delete_query = ''' 
                            DELETE FROM document_embeddings WHERE document_id = %s;

                            '''
            self.cur.execute(delete_query,(id,))
            self.conn.commit()

            return True
        except psycopg2.Error as e:
            print("Error: Unable to delete data")
            print(e)
            self.conn.rollback()
            return False

        
    def close_connection(self):
        if self.conn is not None:
            self.conn.close()
            print("Database connection closed")
